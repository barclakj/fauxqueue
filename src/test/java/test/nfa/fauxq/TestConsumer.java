package test.nfa.fauxq;

import com.couchbase.client.java.Bucket;
import com.nfa.fauxq.Event;
import com.nfa.fauxq.Queue;
import com.nfa.fauxq.consumer.IConsumer;
import com.nfa.fauxq.couchbase.Consumer;
import io.micrometer.core.instrument.MeterRegistry;
import junit.framework.TestCase;
import rx.observables.ConnectableObservable;
import rx.schedulers.Schedulers;

public class TestConsumer extends TestCase {

    private static Bucket bucket = TestProducer.bucket;
    private static MeterRegistry registry = null;


    public void testPopDocument() {
        String queueName = "Q1";
        String consumerName = "Prod1";

        Queue q = new Queue();
        q.setName(queueName);
        q.setEventTtl(60);

        IConsumer consumer = Consumer.newConsumer(bucket, q, null, null , consumerName);

        consumer.get(new DummyListener());

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e2) { }

    }

    public void testListenDocs() {
        DummyListener l = new DummyListener();

        long start = System.currentTimeMillis();

        String queueName = "Q1";
        String consumerName = "Prod3";

        Queue q = new Queue();
        q.setName(queueName);
        q.setLockTtl(1);

        Queue poisonq = new Queue();
        poisonq.setName("POISONQ1");
        poisonq.setEventTtl(600);

        Queue failedq = new Queue();
        failedq.setName("FAILEDQ1");
        failedq.setEventTtl(3600);

        IConsumer consumer = Consumer.newConsumer(bucket, q, failedq, poisonq, consumerName);

        consumer.setAutoCommitOnListen(true);

        consumer.listen(l);

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) { }

        //consumer.commit();


        System.out.println("Handled " + l.count + " records in " + (System.currentTimeMillis()-start) + "ms");
    }

}
