package test.nfa.fauxq;

import com.nfa.fauxq.Event;
import com.nfa.fauxq.consumer.EventConsumerException;
import com.nfa.fauxq.consumer.IEventListener;
import com.nfa.fauxq.consumer.UnacceptableEventException;

import java.util.Objects;

public class DummyListener implements IEventListener {
    public long count = 0;

    @Override
    public boolean handleEvent(Event e) throws UnacceptableEventException, EventConsumerException {
        System.out.println("Handling: " + new String(e.getMessage()));

        if (Objects.equals("POISON MSG", new String(e.getMessage()))) {
            System.err.println("POISON MESSAGE DETECTED!");
            throw new UnacceptableEventException(e.getDocumentKey());
        }

        if (Objects.equals("FAILURE MSG", new String(e.getMessage()))) {
            System.err.println("FAILING MESSAGE DETECTED!");
            throw new EventConsumerException(e.getDocumentKey());
        }

        count++;

        return false;
    }
}
