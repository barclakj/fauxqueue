package test.nfa.fauxq;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.CouchbaseCluster;
import com.nfa.fauxq.Queue;
import com.nfa.fauxq.couchbase.Producer;
import com.nfa.fauxq.producer.IProducer;
import io.micrometer.core.instrument.Clock;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.influx.InfluxConfig;
import io.micrometer.influx.InfluxMeterRegistry;
import junit.framework.TestCase;
import rx.Observable;

import java.time.Duration;

public class TestProducer extends TestCase {

    public static Bucket bucket = null;
    private static MeterRegistry registry = null;

    static {
        Cluster cluster = CouchbaseCluster.create("localhost");
        cluster.authenticate("eventstore", "password");

        bucket = cluster.openBucket("eventstore" );

        InfluxConfig config = new InfluxConfig() {
            @Override
            public Duration step() {
                return Duration.ofSeconds(1);
            }

            @Override
            public String db() {
                return "testdb";
            }

            @Override
            public String get(String k) {
                return null; // accept the rest of the defaults
            }
        };
        registry = new InfluxMeterRegistry(config, Clock.SYSTEM);
        Metrics.addRegistry(registry);
    }

    public void testCreateDocument() {
        String queueName = "Q1";
        String producerName = "Prod1";

        Queue q = new Queue();
        q.setName(queueName);
        q.setEventTtl(60);

        IProducer producer = Producer.newProducer(bucket, q, producerName);

        String msgId = producer.put("This is a simple xxxtest!");
        System.out.println("Document created with id: " + msgId);

        msgId = producer.put("POISON MSG");
        System.out.println("Document created with id: " + msgId);

        msgId = producer.put("FAILURE MSG");
        System.out.println("Document created with id: " + msgId);

        assertTrue(msgId!=null);
    }

    public void testCreateManyDocs() {
        String queueName = "Q1";
        String producerName = "Prod2";
        long start = System.currentTimeMillis();

        Queue q = new Queue();
        q.setName(queueName);
        q.setEventTtl(60);

        IProducer producer = Producer.newProducer(bucket, q, producerName);

        int n = 10000;// 00;
        for(int i=0;i<n;i++) {
            String msgId = producer.put("MSG:" + i);

            System.out.println("Document created with id: " + msgId);

            assertTrue(msgId!=null);

          //  try {
            //    Thread.sleep(10);
           // } catch (InterruptedException ex) { }

        }
        System.out.println("Created " + n + " records in " + (System.currentTimeMillis()-start) + "ms");
    }


    public void testObservable() {
        String[] data = {"one", "two", "three", "four"};

        Observable<String> someObserver = Observable.from(data);

        String queueName = "OBSERVER";
        String producerName = "P3";

        Queue q = new Queue();
        q.setName(queueName);
        q.setEventTtl(60);

        long start = System.currentTimeMillis();

        IProducer producer = Producer.newProducer(bucket, q, producerName);

        producer.watch(someObserver, null);

        System.out.println("Watching...");

        try {
            Thread.sleep(3000);
        } catch (InterruptedException ex) { }
    }
}
