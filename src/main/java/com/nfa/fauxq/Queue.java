package com.nfa.fauxq;

public class Queue {
    private String name = null;
    private int mode = FIFO;
    /**
     * Time allowed for processing before lock is automatically released.
     */
    private int lockTtl = DEFAULT_LOCK_TTL;

    /**
     * Time for which events should be retained.
     */
    private int eventTtl = DEFAULT_EVENT_TTL;

    /**
     * Max number of failures before the message is moved to DLQ.
     */
    private int maxFailures = DEFAULT_MAX_FAILURES;

    public static final int DEFAULT_EVENT_TTL = -1; // forever
    public static final int DEFAULT_LOCK_TTL = 10; // 10 seconds
    public static final int DEFAULT_MAX_FAILURES = 3;

    public static final int FIFO = 0;
    public static final int LIFO = 1;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public boolean isFifo() {
        if (mode==FIFO) return true;
        else return false;
    }

    public boolean isLifo() {
        if (mode==LIFO) return true;
        else return false;
    }

    public int getLockTtl() {
        return lockTtl;
    }

    public void setLockTtl(int lockTtl) {
        this.lockTtl = lockTtl;
    }

    public int getEventTtl() {
        return eventTtl;
    }

    public void setEventTtl(int eventTtl) {
        this.eventTtl = eventTtl;
    }

    public int getMaxFailures() {
        return maxFailures;
    }

    public void setMaxFailures(int maxFailures) {
        this.maxFailures = maxFailures;
    }
}
