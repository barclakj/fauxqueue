package com.nfa.fauxq.consumer;

public interface IConsumer {
    long count();
    void get(IEventListener listener);
    void get(IEventListener listener, long waitMs);

    void setAutoCommitOnListen(boolean autoCommit);
    boolean isAutoCommitOnListen();

    void listen(IEventListener listener);

}
