package com.nfa.fauxq.consumer;

public class EventConsumerException extends Exception {

    public EventConsumerException() {
        super();
    }

    public EventConsumerException(String msg) {
        super(msg);
    }

    public EventConsumerException(Throwable t) {
        super(t);
    }

    public EventConsumerException(String msg, Throwable t) {
        super(msg, t);
    }
}
