package com.nfa.fauxq.consumer;

public class UnacceptableEventException extends Exception {

    public UnacceptableEventException() {
        super();
    }

    public UnacceptableEventException(String msg) {
        super(msg);
    }

    public UnacceptableEventException(Throwable t) {
        super(t);
    }

    public UnacceptableEventException(String msg, Throwable t) {
        super(msg, t);
    }
}
