package com.nfa.fauxq.consumer;

import com.nfa.fauxq.Event;

public interface IEventListener {
    /**
     * Handles an event.
     * Returns true if the event history should be committed. False
     * and the event will be added to history but not committed.
     *
     * If autocommit is true then event will be committed regardless.
     * @param e
     * @return
     * @throws UnacceptableEventException
     * @throws EventConsumerException
     */
    boolean handleEvent(Event e) throws UnacceptableEventException, EventConsumerException;
}
