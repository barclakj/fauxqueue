package com.nfa.fauxq.metrics;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Timer;

import java.util.concurrent.TimeUnit;

public class FQMetrics {
    private static MeterRegistry registry = Metrics.globalRegistry;

    public static Timer getTimer(String name) {
        Timer timer = Timer
                .builder(name)
                .publishPercentiles(0.5, 0.95, 1.0) // median and 95th percentile
                .publishPercentileHistogram()
                .register(registry);
        return timer;
    }

    public static void recordTimer(String timerName, long startTimeNanos) {
        Timer timer = getTimer(timerName);
        timer.record(System.nanoTime()-startTimeNanos, TimeUnit.NANOSECONDS);
    }


    public static Counter getCounter(String name) {
        Counter c = Counter.builder(name).register(registry);
        return c;
    }

    public static void incrementCounter(String name) {
        getCounter(name).increment();
    }

}
