package com.nfa.fauxq;

import java.util.UUID;

public class Event {
    protected String producer = null;
    protected String correlationId = null;
    public String documentKey = null;
    protected long createdTimestampNs = 0l;
    protected byte[] message = null;
    protected String contentType = null;
    private long seq = -1;
    private String topicSpace = null;

    public static final String DEFAULT_CONTENT_TYPE = "application/octetstream";

    protected Event() {
        super();
    }

    public static Event newEvent(String producer, byte[] msg) {
            return newEvent(producer, msg, DEFAULT_CONTENT_TYPE);
    }

    public static Event newEvent(String producer, byte[] msg, String contentType) {
        Event e = new Event();
        e.correlationId = UUID.randomUUID().toString();
        e.producer = producer;
        e.createdTimestampNs = System.nanoTime();
        e.message = msg;
        e.contentType = contentType;
        return e;
    }

    public static Event newEvent(String producer, byte[] msg, String contentType, String correlationId, long ts, String docKey, long seq, String topicSpace) {
        Event e = new Event();
        e.correlationId = correlationId;
        e.producer = producer;
        e.createdTimestampNs = ts;
        e.message = msg;
        e.contentType = contentType;
        e.documentKey = docKey;
        e.seq = seq;
        e.topicSpace = topicSpace;
        return e;
    }

    public void setDocumentKey(String k) {
        this.documentKey = k;
    }

    public String getDocumentKey() {
        return documentKey;
    }

    public String getCorrelationId() {
        return correlationId;
    }

    public String getProducer() {
        return producer;
    }

    public long getCreatedTimestampNs() {
        return createdTimestampNs;
    }

    public byte[] getMessage() {
        return message;
    }

    public String getContentType() {
        return contentType;
    }

    public long getSeq() {
        return seq;
    }

    public void setSeq(long seq) {
        this.seq = seq;
    }

    public String getTopicSpace() {
        return topicSpace;
    }

    public void setTopicSpace(String topicSpace) {
        this.topicSpace = topicSpace;
    }
}

