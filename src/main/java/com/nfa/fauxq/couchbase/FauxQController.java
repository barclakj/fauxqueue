package com.nfa.fauxq.couchbase;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.JsonLongDocument;
import com.couchbase.client.java.document.json.JsonObject;
import com.couchbase.client.java.error.CASMismatchException;
import com.couchbase.client.java.error.DocumentAlreadyExistsException;
import com.couchbase.client.java.error.DocumentDoesNotExistException;
import com.nfa.fauxq.Event;
import com.nfa.fauxq.Queue;
import com.nfa.fauxq.consumer.EventConsumerException;
import com.nfa.fauxq.consumer.IEventListener;
import com.nfa.fauxq.consumer.UnacceptableEventException;
import com.nfa.fauxq.metrics.FQMetrics;
import io.micrometer.core.instrument.Timer;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FauxQController {
    private static Logger log = Logger.getLogger(FauxQController.class.getSimpleName());

    /**
     * Random reference for the current controller instance.
     */
    private String controllerRef = UUID.randomUUID().toString();
    /**
     * Bucket to use...
     */
    private Bucket bucket = null;

    /**
     * Keep track of the previous max so we can save DB hits on no activity.
     */
    private long oldMax = -1;

    /**
     * Name of the consumer.
     */
    private String consumerName = null;

    private Timer handleEventTimer = FQMetrics.getTimer("fauxqueue.consumer.handleevent");

    private Map<String, Event> eventHistory = new HashMap<>();

    public void configure(final Bucket bucket, final String consumerName) {
        this.bucket = bucket;
        this.consumerName = consumerName;
    }

    public long consumeDocuments(final Queue queue, final IEventListener listener) {
        long count = 0;
        // get the range.
        long max = getLatest(queue);
        if (max!=oldMax) { // if max has not changed and last time we did nothing then still do nothing
            long min = getNext(queue);
            long minToMark = min;
            if (log.isLoggable(Level.FINE)) log.fine(String.format("Range is from %d to %d", min, max));

            // iterate over range.
            if (min <= max) {
                oldMax = -1;
                for (long i = min; i <= max; i++) {
                    if (log.isLoggable(Level.FINE)) log.fine("Processing document by index:  " + i);
                    if (!isDocumentDone(queue, i)) {
                        log.fine("Creating lock");
                        // create a lock document
                        if (createLock(queue, i)) {
                            log.fine("Lock created");
                            // ok doc is good.
                            JsonDocument doc = bucket.get(DocumentNames.createQueueKeyName(queue, i));
                            log.fine("Got doc");
                            Event e = EventTranslator.contentToEvent(doc);
                            handleEventTimer.record(() -> {
                                handleEvent(e, listener, queue);
                            });
                            minToMark = handleDone(i, minToMark, queue);
                            count++;
                        } else {
                            log.fine("Failed to create lock!"); // this is ok, get the next one..
                        }
                    } else {
                        // see if we can update to done
                        minToMark = handleDone(i, minToMark, queue);
                    }
                }
            } else {
                oldMax = max;
                // don't bother doing anything if no new events arrived...
            }
            log.fine("Get complete!");
        }
        return count;
    }

    private long handleDone(final long i, final long minToMark, final Queue queue) {
        long newMin = minToMark;
        log.fine("Doc is done!...");
        // set the last successful one done.
        if (i == minToMark) {
            log.fine("Can increment consumer index...");
            // can only update if its the minimum one expected
            // since we may have other threads doing a lower
            // index which may yet fail!
            updateQueueConsumerIndex(queue, i);
            newMin = minToMark+1; // can do the next one as well if possible...
            log.fine("Queue increment done.");
            //min = min + 1; // since we know "min" is done, the next one can also be marked done if we like...
            FQMetrics.incrementCounter("fauxqueue.eventlock.done");
            //break;
        } else {
            log.fine("CanNOT mark as doc done!...");
            if (log.isLoggable(Level.FINE))
                log.fine(String.format("Refusing to increment counter since %d!=%d", i, minToMark));
            FQMetrics.incrementCounter("fauxqueue.eventlock.jump");
        }
        return newMin;
    }

    private void handleEvent(final Event e, final IEventListener listener, final Queue queue) {
        if (e != null) {
            if (log.isLoggable(Level.FINE)) log.fine("Processing event: " + e.getDocumentKey());
            try {
                boolean commit = listener.handleEvent(e);
                eventHistory.put(e.getDocumentKey(), e);
                if (commit) {
                    commit(queue); // will commit all to date
                }
            } catch (UnacceptableEventException ex) {
                log.log(Level.WARNING, " UnacceptableEventException exception occurred whilst processing messaga. Will continue...: " + ex.getMessage(), ex);
                log.log(Level.WARNING, "UnacceptableEventException for document: " + e.getDocumentKey());
            } catch (EventConsumerException ex) {
                this.markDocumentFailed(queue, e.getSeq());
            } finally {
                FQMetrics.incrementCounter("fauxqueue.consumer.get.counter");
            }
        }
    }

    private void commit(final Queue queue) {
        long start = System.nanoTime();
        synchronized (eventHistory) {
            eventHistory.values().stream()
                    .forEach((e) -> commit(e, false, queue));
            eventHistory.clear();
        }
        FQMetrics.recordTimer("fauxqueue.consumer.bulkcommit", start);
    }

    private void commit(Event e, boolean clearHistory, final Queue queue) {
        long start = System.nanoTime();

        if (log.isLoggable(Level.FINE)) log.fine( String.format("Commiting transaction for %s/%s and index %d.",queue.getName(), consumerName, e.getSeq()));
        this.markDocumentDone(queue, e.getSeq());
        if (clearHistory) eventHistory.remove(e.getDocumentKey());
        FQMetrics.recordTimer("fauxqueue.consumer.commit", start);
    }

    /**
     * Updates the last index for the queue/consumer combo to
     * the last known "done" index value.
     * @param q
     * @param idx
     */
    private void updateQueueConsumerIndex(final Queue q, final long idx) {
        String docKey = DocumentNames.formQueueConsumerName(q, consumerName);

        try {
            JsonDocument doc = bucket.get(docKey);
            if (doc!=null) {
                long val = doc.content().getLong("last");
                if (val<idx) {
                    doc.content().put("last", idx);
                    bucket.replace(doc);
                } else {
                    // value is already greater than or equal to current index! someone else has this msg..
                    log.fine( String.format("Queue/consumer index %s/%s already greater than expected %d>=%d.", q.getName(), consumerName, val, idx));
                }
            } else {
                // create the doc.
                JsonObject jo = JsonObject.create();
                jo.put("last", idx);
                bucket.insert(JsonDocument.create(docKey, jo));
            }
        } catch (CASMismatchException e) {
            log.fine(String.format("CAS mismatch on queue %s, consumer %s for index %d", q.getName(), consumerName, idx));
        } catch (DocumentAlreadyExistsException e) {
            log.fine(String.format("Document already exists on queue %s for consumer %s", q.getName(), consumerName));
        }
    }



    /**
     * Creates a lock document for the specified index, consumer and queue.
     * @param q
     * @param idx
     * @return
     */
    private boolean createLock(final Queue q, final long idx) {
        boolean locked = false;
        String lockName = DocumentNames.createLockDocName(q, consumerName, idx);
        JsonObject lockDoc = JsonObject.create();
        lockDoc.put("c", consumerName);
        lockDoc.put( "idx", idx);
        lockDoc.put( "q", q.getName());
        lockDoc.put( "ref", this.controllerRef);
        lockDoc.put( "ts", System.nanoTime());

        JsonDocument doc = JsonDocument.create(lockName, q.getLockTtl(), lockDoc);

        try {
            bucket.insert(doc);
            locked = true;
        } catch (DocumentAlreadyExistsException e) {
            if (log.isLoggable(Level.FINE)) log.fine(String.format("Lock already exists for %s", lockName));
            FQMetrics.incrementCounter("fauxqueue.eventlock.conflict");
        }
        return locked;
    }

    /**
     * Checks to see if the document has already been consumed
     * by the specified consumer. If it doesn't exist then it's
     * also considered to be consumed already.
     * @param queue
     * @param idx
     * @return
     */
    private boolean isDocumentDone(final Queue queue, final long idx) {
        boolean done = false;
        String docKey = DocumentNames.createQueueKeyName(queue, idx);

        JsonDocument doc = bucket.get(docKey);
        if (doc==null) {
            done = true; // its done if the actual doc doesn't exist!
        } else {
            String queueConsumerIndexKey = DocumentNames.createQueueConsumerIndexDone(queue, consumerName, idx);
            JsonDocument doneDoc = bucket.get(queueConsumerIndexKey);
            if (doneDoc!=null) {
                // done doc exists... so we've done it!
                done = true;
            }
        }
        if (done) {
            if (log.isLoggable(Level.FINE)) log.fine(String.format("Appears that document has been done for doc %s and consumer %s at index %d", docKey, consumerName, idx));
        } else {
            if (log.isLoggable(Level.FINE)) log.fine(String.format("Document needs processing for %s and consumer %s and index %d", docKey, consumerName, idx));
        }
        return done;
    }

    /**
     * Creates a document done marker.
     * @param queue
     * @param idx
     */
    public void markDocumentDone(final Queue queue, final long idx) {
        String queueConsumerIndexKey = DocumentNames.createQueueConsumerIndexDone(queue, consumerName, idx);
        JsonObject jo = JsonObject.create();
        jo.put("ts", System.nanoTime());
        try {
            JsonDocument doc = null;
            if (queue.getEventTtl()>0) {
                doc = bucket.insert(JsonDocument.create(queueConsumerIndexKey, queue.getEventTtl(), jo));
            } else {
                doc = bucket.insert(JsonDocument.create(queueConsumerIndexKey, jo));
            }
            FQMetrics.incrementCounter("fauxqueue.consumer.done");
        } catch (DocumentAlreadyExistsException e) {
            log.severe(String.format("Potential duplicate processing for queue/consumer and doc: %s/%s/and %d",queue.getName(), consumerName, idx));
        }
    }

    /**
     * Creates a document done marker.
     * Returns true if the document should go to the failure queue.
     * @param queue
     * @param idx
     */
    public boolean markDocumentFailed(final Queue queue, final long idx) {
        long failureCount = incrementFailureCounter(queue, idx);
        if (failureCount>queue.getMaxFailures()) {
            // mark as done.
            markDocumentDone(queue, idx);
            return true;
        } else return false;
    }



    /**
     * Returns the latest record number for the queue.
     * @param q
     * @return
     */
    private long getLatest(final Queue q) {
        long currentMax = -1;
        if (log.isLoggable(Level.FINE)) log.fine(String.format("Find latest event number for queue %s", q.getName()));
        try {
            JsonLongDocument doc = bucket.get(q.getName(), JsonLongDocument.class);
            if (doc!=null) {
                currentMax = bucket.get(q.getName(), JsonLongDocument.class).content();
            }
        } catch (DocumentDoesNotExistException e) {
            // ok, not existing.. will be minus 1.
        }
        return currentMax;
    }

    /**
     * Returns the next record not to have been consumed by the consumer on the queue.
     * @param q
     * @return
     */
    private long getNext(final Queue q) {
        if (log.isLoggable(Level.FINE)) log.fine(String.format("Find last processed event for queue %s and consumer %s", q.getName(), consumerName));
        long min = 0;
        try {
            JsonDocument doc = bucket.get( DocumentNames.formQueueConsumerName(q, consumerName));
            if (doc!=null) {
                min = doc.content().getLong("last") + 1;
            }
        } catch (DocumentDoesNotExistException e) {
            // not found
            min = 0;
        }
        return min;
    }



    /**
     * Increments the failure counter by 1 for specified queue.
     * @param q
     * @param idx
     * @return
     */
    private long incrementFailureCounter(final Queue q, final long idx) {
        long c = 0;
        String keyName = DocumentNames.createFailureCounterName(q, consumerName, idx);
        if (log.isLoggable(Level.INFO)) log.info(String.format("Incrementing failure counter for queue %s, consumer %s and record %d", q.getName(), consumerName, idx));
        c = bucket.counter(keyName, 1l, 1l).content();
        return c;
    }


    /**
     * Increments the counter by 1 for specified queue.
     * @param q
     * @return
     */
    private long incrementCounter(final Queue q) {
        long c = -1;
        if (log.isLoggable(Level.FINE)) log.fine(String.format("Incrementing counter for queue %s", q.getName()));

        c = bucket.counter(q.getName(), 1l, 1l).content();

        return c;
    }



    /**
     * Creates a new document from the provided event.
     * @param q
     * @param e
     * @return
     */
    public String createDocument(final Queue q, final Event e) {
        if (log.isLoggable(Level.FINE)) log.fine(String.format("Creating document in queue %s with correlation id %s", q.getName(), e.getCorrelationId()));

        long counter = incrementCounter(q);
        String docKey = DocumentNames.createQueueKeyName(q, counter);

        JsonDocument doc = buildEventDocument(q, e, docKey, counter);

        bucket.insert(doc);

        return docKey;
    }

    /**
     * Builds an event json document from provided data.
     * @param q
     * @param e
     * @param docKey
     * @return
     */
    private static JsonDocument buildEventDocument(final Queue q, final Event e, final String docKey, final long counter) {
        JsonObject json = EventTranslator.eventToDocument(e, docKey, counter, q.getName());

        if (q.getEventTtl()>0) {
            return JsonDocument.create(docKey, q.getEventTtl(), json);
        } else {
            return JsonDocument.create(docKey, json);
        }
    }
}
