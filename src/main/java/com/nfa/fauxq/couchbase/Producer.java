package com.nfa.fauxq.couchbase;

import com.couchbase.client.java.Bucket;
import com.nfa.fauxq.Event;
import com.nfa.fauxq.Queue;
import com.nfa.fauxq.metrics.FQMetrics;
import com.nfa.fauxq.producer.IProducer;
import rx.Observable;

import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Producer implements IProducer {
    private static Logger log = Logger.getLogger(Producer.class.getSimpleName());

    protected FauxQController controller = null;
    protected Queue queue = null;
    protected String producerName = null;

    protected Producer() {
        super();
    }

    @Override
    public void watch(final Observable<String> observable, final String topic) {
        Executors.newCachedThreadPool().submit(() -> {
                observable.subscribe((s) -> {
                    String id = this.put(s, topic);
                    if (log.isLoggable(Level.INFO)) log.info(s + " -> " + id);
                },
                Throwable::printStackTrace,
                () -> log.warning("Producer subscription terminated"));
            }
        );
    }

    @Override
    public String put(final Event e) {
        long start = System.nanoTime();
        String id = controller.createDocument(queue, e);
        FQMetrics.recordTimer("fauxqueue.producer.put", start);
        return id;
    }

    @Override
    public String put(final byte[] message, final String topic) {
        Event e = Event.newEvent(producerName, message);
        if (topic!=null) e.setTopicSpace(topic);
        return put(e);
    }

    public static IProducer newProducer(Bucket bucket, Queue queue, String producerName) {
        long start = System.nanoTime();
        log.fine(String.format("Creating producer %s for queue %s", producerName, queue.getName()));
        FauxQController cbController = new FauxQController();
        cbController.configure(bucket, null);

        Producer producer = new Producer();
        producer.controller = cbController;
        producer.producerName = producerName;
        producer.queue = queue;

        FQMetrics.recordTimer("fauxqueue.producer.create", start);

        return producer;
    }
}
