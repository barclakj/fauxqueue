package com.nfa.fauxq.couchbase;

import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.json.JsonObject;
import com.nfa.fauxq.Event;

import java.util.Base64;

class EventTranslator {

    /**
     * b64 encoder
     */
    private static Base64.Encoder encoder = Base64.getEncoder();
    /**
     * b64 decoder
     */
    private static Base64.Decoder decoder = Base64.getDecoder();


    /**
     * Converts an event to a JSON object for storage.
     * @param e
     * @param docKey
     * @param counter
     * @param queueName
     * @return
     */
    public static JsonObject eventToDocument(final Event e, final String docKey, final long counter, final String queueName) {
        JsonObject json = JsonObject.create();
        json.put("correlationId", e.getCorrelationId());
        json.put("ts", e.getCreatedTimestampNs());
        json.put("msg",new String(encoder.encode(e.getMessage())) );
        json.put("contentLength", e.getMessage().length);
        json.put("contentType", e.getContentType());
        json.put("id", docKey);
        json.put("q", queueName);
        json.put("producer", e.getProducer());
        json.put("seq", counter);
        json.put("topicSpace", e.getTopicSpace());
        return json;
    }

    /**
     * Converts a CB document to an event.
     * @param doc
     * @return
     */
    public static Event contentToEvent(final JsonDocument doc) {
        String id = doc.id();
        JsonObject content = doc.content();
        String contentType = content.getString("contentType");
        long contentLength = content.getLong("contentLength");
        String producer = content.getString("producer");
        byte[] msg = decoder.decode(content.getString("msg").getBytes());
        if (msg.length!=contentLength) throw new ArrayIndexOutOfBoundsException("Message does not match expected length!");
        long ts = content.getLong("ts");
        long seq = content.getLong("seq");
        String correlationId = content.getString("correlationId");

        String topicSpace = content.getString("topicSpace");

        Event e = Event.newEvent(producer, msg, contentType, correlationId, ts, id, seq, topicSpace);
        return e;
    }
}
