package com.nfa.fauxq.couchbase;

import com.couchbase.client.java.Bucket;
import com.nfa.fauxq.Queue;
import com.nfa.fauxq.consumer.IConsumer;
import com.nfa.fauxq.consumer.IEventListener;
import com.nfa.fauxq.metrics.FQMetrics;
import com.nfa.fauxq.producer.IProducer;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Consumer implements IConsumer {
    private static Logger log = Logger.getLogger(Producer.class.getSimpleName());

    protected FauxQController controller = null;
    protected Queue queue = null;
    protected IProducer failedMessageProducer = null;
    protected IProducer poisonMessageProducer = null;
    protected String consumerName = null;

    /**
     * Indicates if transactions should be autocommited.
     */
    private boolean autoCommit = true;

    private long c = 0;

    public static final long WAIT_TIME_FOR_EVENT_MS = 250;

    protected Consumer() {
        super();
    }

    @Override
    public long count() {
        return c;
    }

    @Override
    public void setAutoCommitOnListen(boolean autoCommit) {
        this.autoCommit = autoCommit;
    }

    @Override
    public boolean isAutoCommitOnListen() {
        return this.autoCommit;
    }

    public static IConsumer newConsumer(final Bucket bucket, final Queue queue, final Queue failedOverflowQueue, final Queue poisonQueue, final String consumerName) {
        long start = System.nanoTime();
        log.info(String.format("Creating consumer %s for queue %s", consumerName, queue.getName()));
        FauxQController cbController = new FauxQController();
        cbController.configure(bucket, consumerName);

        Consumer consumer = new Consumer();
        consumer.controller = cbController;
        consumer.consumerName = consumerName;
        consumer.queue = queue;

        if (failedOverflowQueue!=null) {
            consumer.failedMessageProducer = Producer.newProducer(bucket, failedOverflowQueue, consumerName);
        }
        if (poisonQueue!=null) {
            consumer.poisonMessageProducer = Producer.newProducer(bucket, poisonQueue, consumerName);
        }

        FQMetrics.recordTimer("fauxqueue.consumer.create", start);

        return consumer;
    }

    @Override
    public void get(IEventListener listener) {
        get(listener, WAIT_TIME_FOR_EVENT_MS);
    }

    @Override
    public void get(IEventListener listener, long waitMs) {
        long start = System.nanoTime();

        if (log.isLoggable(Level.FINE))
            log.fine(String.format("Fetching next document from queue %s", queue.getName()));

        long count = controller.consumeDocuments(queue, listener);
        if (count>0) {
            FQMetrics.recordTimer("fauxqueue.consumer.get", start);
        }
        c = c + count;
    }

    @Override
    public void listen(IEventListener listener) {
        ExecutorService exec = Executors.newCachedThreadPool();

        exec.submit(() -> {
            try {
                boolean carryOn = true;
                while (carryOn) {
                    get(listener);
                    try {
                        Thread.sleep(WAIT_TIME_FOR_EVENT_MS);
                    } catch (InterruptedException ex) {
                        log.severe(".............. FauxQueue Listener Interrupted! Stopping! ...............");
                        carryOn = false;
                    }
                }
            } catch (Throwable t) {
                t.printStackTrace();
                throw t;
            } finally {
                log.severe("Event Listener Stopped!");
            }
        });

    }
}
