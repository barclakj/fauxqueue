package com.nfa.fauxq.couchbase;

import com.nfa.fauxq.Queue;

class DocumentNames {

    /**
     * Returns queue key name for specified index.
     * @param q
     * @param idx
     * @return
     */
    public static String createQueueKeyName(final Queue q, final long idx) {
        return q.getName() + ":" + idx;
    }

    /**
     * Returns a failure document name for the queue, consumer and index.
     * @param q
     * @param consumer
     * @param idx
     * @return
     */
    public static String createFailureCounterName(final Queue q, String consumer, final long idx) {
        return consumer + ":" + q.getName() + ":" + idx + ":FAILURES";
    }

    /**
     * Returns the queue and consumer name for document.
     * @param q
     * @param consumerName
     * @return
     */
    public static String formQueueConsumerName(final Queue q, final String consumerName) {
        return consumerName + ":" + q.getName();
    }


    /**
     * Returns a lock name for the doc.
     * @param q
     * @param consumer
     * @param idx
     * @return
     */
    public static String createLockDocName(final Queue q, final String consumer, final long idx) {
        return consumer + ":" + q.getName() + ":" + idx + ":LOCK";
    }

    /**
     * Returns a done document name for the doc.
     * @param q
     * @param consumer
     * @param idx
     * @return
     */
    public static String createQueueConsumerIndexDone(final Queue q, final String consumer, final long idx) {
        return consumer + ":" + q.getName() + ":" + idx + ":DONE";
    }
}
