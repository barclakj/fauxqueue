package com.nfa.fauxq.producer;

import com.nfa.fauxq.Event;
import rx.Observable;

public interface IProducer {

    void watch(final Observable<String> observable, final String topic);

    String put(final byte[] message, final String topic);

    String put(final Event e);

    default String put(final String message, final String topic) {
        if (message==null) throw new NullPointerException();
        else return put(message.getBytes(), topic);
    }

    default String put(final byte[] message) {
        if (message==null) throw new NullPointerException();
        else return put(message, null);
    }

    default String put(final String message) {
        if (message==null) throw new NullPointerException();
        else return put(message.getBytes(), null);
    }

    default void watch(final Observable<String> observable) {
        watch(observable, null);
    }
}
